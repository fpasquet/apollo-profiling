const { RESTDataSource: BaseRESTDataSource } = require('apollo-datasource-rest');

class RESTDataSource extends BaseRESTDataSource {
    initialize(config) {
        super.initialize(config);
        this.profiler = config.context.profiler;
    }

    willSendRequest(request) {
        this.startTime = process.hrtime();
        if (this.profiler) {
            request.headers.set('x-token-global', this.profiler.tokenGlobal);
            request.headers.set('x-token-process', this.profiler.tokenProcess);
        }
    }

    didReceiveResponse(response, request) {
        const infoTrace = {
            url: request.url,
            isCached: !response.url,
            method: request.method,
            headers: request.headers,
            status: response.status,
            statusText: response.statusText,
        };
        return super
            .didReceiveResponse(response, request)
            .then(body => ({ infoTrace, body }))
            .catch(error => ({
                infoTrace,
                error,
            }));
    }

    trace(label, fn) {
        if (this.profiler) {
            return this.profiler.traceRESTDataSource(fn);
        }
        return fn().then(({ body, error }) => {
            if (error) {
                throw error;
            }
            return body;
        });
    }
}

module.exports = RESTDataSource;
