# Apollo Profiling

## Setup on Apollo Server:

```js
const { ProfilerExtension, Profiler } = require('./profiler');

const server = new ApolloServer({
    ...options,
    extensions: [() => new ProfilerExtension()],
    context: ({ req, res }) => ({
      profiler: new Profiler({ req }),
    }),
  });
```

Extend all your RestDataSource

```js
const BaseRESTDataSource = require('./dataSources/RESTDataSource');

class MovieRESTDataSource extends BaseRESTDataSource {}
```