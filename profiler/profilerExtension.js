class ProfilerExtension {
  requestDidStart({ request, queryString, operationName, variables, context, persistedQueryHit, requestContext }) {
    this.profiler = context.profiler;
    this.profiler.initialize({
      method: request.method,
      headers: request.headers,
      queryString: persistedQueryHit ? requestContext.source : queryString,
      operationName,
      variables,
    });
  }

  executionDidStart() {
    return () => {
      this.profiler.stopTiming();
    };
  }

  willResolveField(source, args, context, info) {
    return this.profiler.resolveField(info);
  }

  willSendResponse(args) {
    const level = args.graphqlResponse.errors ? 'error' : 'info';
    this.profiler.log(level);
    return args;
  }

  format() {
    this.profiler.profilingEnd();

    return ['dev', 'demo', 'qualif'].includes(process.env.STAGE)
      ? ['profiler', this.profiler.formatForExtension()]
      : [];
  }
}

module.exports = ProfilerExtension;
