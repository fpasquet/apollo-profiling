const logger = require('gelf-pro');

const Logger = ({ tokenGlobal, tokenProcess, unitOfTime }) => {
  const stage = process.env.STAGE || 'dev';

  logger.setConfig({
    fields: {
      type: 'gelf',
      stage,
      facility: 'app',
      application: 'gateway',
      host: `${stage}-gateway`,
      token_global: tokenGlobal,
      token_process: tokenProcess,
    },
    adapterOptions: {
      host: process.env.LOGSTASH_HOST,
      port: process.env.LOGSTASH_PORT,
      protocol: 'udp4',
    },
    broadcast: [
      message => {
        if (message) {
          const {
            level,
            ctx: { graphql, data_sources },
          } = message;

          let logging = `\n\n--------- START_MONITORING_GRAPHQL ---------\n`;
          logging += `GraphQL operation name: ${graphql.operation_name}\n`;

          if (data_sources && data_sources.rest && data_sources.rest.requests) {
            logging += `RestDataSource: \n`;
            logging += `Number of requests: ${data_sources.rest.number_of_requests}\n`;
            logging += `Duration min request: ${data_sources.rest.duration.min} ${unitOfTime}\n`;
            logging += `Duration max request: ${data_sources.rest.duration.max} ${unitOfTime}\n`;
            logging += `Duration total request: ${data_sources.rest.duration.total} ${unitOfTime}\n`;
            const requests = JSON.parse(data_sources.rest.requests);
            for (const request of requests) {
              logging += `Requests REST: ${request.method} ${request.url} ${request.duration} ${unitOfTime}\n`;
            }
          }

          logging += `--------- END_MONITORING_GRAPHQL ---------\n`;

          if (logging.length > 0) {
            console[level > 3 ? 'log' : 'error'](logging);
          }
        }
      },
    ],
  });

  return logger;
};

module.exports = Logger;
