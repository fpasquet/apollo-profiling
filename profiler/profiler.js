const gql = require('graphql-tag');
const { responsePathAsArray } = require('graphql');
const { defaultEngineReportingSignature, operationHash } = require('apollo-graphql');
const uniqid = require('uniqid');
const dotProp = require('dot-prop');
const Logger = require('./logger');

class Profiler {
  constructor({ req: { method, headers, unitOfTime = 'ms' } }) {
    this.isEnable = true;
    this.unitOfTime = unitOfTime;
    this.tokenGlobal = headers['x-token-global'] ? headers['x-token-global'] : uniqid('global_');
    this.tokenProcess = uniqid('process_');
    this.logger = Logger({
      tokenGlobal: this.tokenGlobal,
      tokenProcess: this.tokenProcess,
      unitOfTime,
    });

    this.profilingData = {
      http: {
        method: method,
        headers: headers,
      },
      graphqlInfo: {},
      rootResolver: {},
      dataSources: {},
    };

    if (headers['client-name']) {
      this.profilingData.graphqlInfo.clientName = headers['client-name'];
    }

    if (headers['client-version']) {
      this.profilingData.graphqlInfo.clientVersion = headers['client-version'];
    }
  }

  initialize({ queryString, operationName, variables }) {
    this.isEnable = operationName !== 'IntrospectionQuery';

    if (!this.isEnable) return;

    queryString = defaultEngineReportingSignature(
      gql`
        ${queryString}
      `,
      operationName,
    );

    this.profilingData.graphqlInfo = {
      ...this.profilingData.graphql,
      operationName,
      queryHash: operationHash(queryString),
      queryString,
      variables,
    };
    this.dataSources = { RESTDataSource: [] };
    this.startTiming();
  }

  startTiming() {
    this.startWallTime = new Date();
    this.startHrTime = process.hrtime();
  }

  stopTiming() {
    if (!this.isEnable) return;

    this.duration = this.durationHrTime(process.hrtime(this.startHrTime));
    this.endWallTime = new Date();
  }

  roundDuration(duration) {
    return Number(duration.toFixed(3));
  }

  durationHrTime(hrtime) {
    const nanoSeconds = hrtime[0] * 1e9 + hrtime[1];
    if (this.unitOfTime === 'ns') {
      return nanoSeconds;
    }
    return this.roundDuration(nanoSeconds / 1e6);
  }

  responsePathAsString(p) {
    if (p === undefined) {
      return '';
    }
    return responsePathAsArray(p).join('.');
  }

  resolveField(info) {
    if (!this.isEnable) return;

    const path = info.path;
    const pathAsString = this.responsePathAsString(path);
    let resolver = {
      path: pathAsString,
      fieldName: path.key.toString(),
    };
    if (typeof path.key === 'string' && path.key !== info.fieldName) {
      resolver = { ...resolver, originalFieldName: info.fieldName };
    }

    resolver = {
      ...resolver,
      parentType: info.parentType,
      type: info.returnType,
      startTime: this.durationHrTime(process.hrtime(this.startHrTime)),
    };

    return () => {
      const endTime = this.durationHrTime(process.hrtime(this.startHrTime));
      const duration = this.roundDuration(endTime ? endTime - resolver.startTime : 0);
      resolver = { ...resolver, endTime, duration };
      dotProp.set(this.profilingData.rootResolver, pathAsString, resolver);
    };
  }

  traceRESTDataSource(fn) {
    if (!this.profilingData.dataSources.RESTDataSource) {
      this.profilingData.dataSources.RESTDataSource = {
        duration: {
          max: null,
          min: null,
          total: 0,
        },
        requests: [],
      };
    }

    const dataSource = {
      startTime: this.durationHrTime(process.hrtime(this.startHrTime)),
    };

    return fn().then(({ infoTrace, body, error }) => {
      const { duration: profilingDataDuration } = this.profilingData.dataSources.RESTDataSource;
      const endTime = this.durationHrTime(process.hrtime(this.startHrTime));
      const duration = this.roundDuration(endTime ? endTime - dataSource.startTime : 0);

      if (!profilingDataDuration.min || duration < profilingDataDuration.min) {
        this.profilingData.dataSources.RESTDataSource.duration.min = duration;
      }

      if (!profilingDataDuration.max || duration > profilingDataDuration.max) {
        this.profilingData.dataSources.RESTDataSource.duration.max = duration;
      }

      this.profilingData.dataSources.RESTDataSource.duration.total += duration;

      this.profilingData.dataSources.RESTDataSource.requests.push({
        ...infoTrace,
        ...dataSource,
        endTime,
        duration,
      });

      if (error) {
        throw error;
      }

      return body;
    });
  }

  profilingEnd() {
    if (!this.isEnable) return;

    const { graphqlInfo, dataSources } = this.profilingData;
    this.profilingData.graphqlInfo = {
      ...graphqlInfo,
      startTime: this.startWallTime,
      endTime: this.endWallTime,
      duration: this.duration,
    };

    if (dataSources) {
      const { RESTDataSource } = dataSources;

      if (RESTDataSource) {
        const numberOfRequests = RESTDataSource.requests.length;
        this.profilingData.dataSources.RESTDataSource = {
          duration: {
            min: RESTDataSource.duration.min,
            max: RESTDataSource.duration.max,
            total: this.roundDuration(RESTDataSource.duration.total),
          },
          numberOfRequests,
          requests: RESTDataSource.requests.map(request => ({
            ...request,
            duration: request.duration,
          })),
        };
      }
    }
  }

  log(level) {
    if (!this.isEnable) return;

    const { http, graphqlInfo, dataSources } = this.profilingData;
    const ctx = {
      http: {
        method: http.method,
        headers: http.headers && Object.keys(http.headers).length > 0 ? JSON.stringify(http.headers, null, 4) : null,
      },
      graphql: {
        operation_name: graphqlInfo.operationName,
        query_hash: graphqlInfo.queryHash,
        query_string: graphqlInfo.queryString,
        variables:
          graphqlInfo.variables && Object.keys(graphqlInfo.variables).length > 0
            ? JSON.stringify(graphqlInfo.variables, null, 4)
            : null,
        start_time: graphqlInfo.startTime.toISOString(),
        end_time: graphqlInfo.endTime.toISOString(),
        duration: graphqlInfo.duration,
      },
    };

    if (dataSources) {
      const { RESTDataSource } = dataSources;
      ctx.data_sources = {};
      if (RESTDataSource) {
        ctx.data_sources.rest = {
          number_of_requests: RESTDataSource.numberOfRequests,
          duration: RESTDataSource.duration,
          requests:
            RESTDataSource.requests && RESTDataSource.requests.length > 0
              ? JSON.stringify(RESTDataSource.requests, null, 4)
              : null,
        };
      }
    }

    this.logger[level](level, {
      ctx,
    });
  }

  formatForExtension() {
    if (
      !this.isEnable ||
      typeof this.startWallTime === 'undefined' ||
      typeof this.endWallTime === 'undefined' ||
      typeof this.duration === 'undefined'
    ) {
      return;
    }

    return {
      http: this.profilingData.http,
      graphqlInfo: this.profilingData.graphqlInfo,
      dataSources: this.profilingData.dataSources,
      rootResolver: this.profilingData.rootResolver,
    };
  }
}

module.exports = Profiler;
